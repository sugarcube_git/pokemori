# pokemori
my private sandbox for vue framework.  
filtering campers by themes and others.  
using bootstrap for UI.  
optimized for smartphone.
___
# pokemori-dist
dist repository.  
add distribution files only by running `npm run publish` from `pokemori` repository.  
`pokemori-dist` repository mirros the server.
___

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
