#!/bin/bash

# フロントエンドプロジェクトの配布用ビルド結果格納パス
SRC_DIR=$1
# 配布用プロジェクトのパス
DIST_DIR=$2
# プッシュ先のリモート名
REMOTE_NAME=$3
# pushするブランチ名
BRANCH_NAME=$4

if [ $# -ne 4 ]; then
  echo "更新対象となるブランチ名を指定して下さい。"
  exit 1
fi

if [ ! -e $DIST_DIR ]; then
  echo "${DIST_DIR}が存在しません。`basename ${DIST_DIR}`をこのプロジェクトと同列の場所にcheckoutしてください。"
  exit 1
fi

pushd $DIST_DIR

echo "${BRANCH_NAME}をチェックアウト..."

git checkout $BRANCH_NAME

if [ $? -gt 0 ]; then
  echo "ブランチの切り替えに失敗しました。ブランチ名に誤りがないか、切り替えられる状態になっている確認してください。"
  exit 1
fi

echo "${BRANCH_NAME}をチェックアウト（完了）"

echo "${DIST_DIR}内をクリーンアップ..."

ls -la1 | grep -v -E '.git$' | xargs rm -r

echo "${DIST_DIR}内をクリーンアップ（完了）"

popd

echo "${SRC_DIR}内を${DIST_DIR}へコピー..."

cp -pr $SRC_DIR/* $DIST_DIR

echo "${SRC_DIR}内を${DIST_DIR}へコピー（完了）"

pushd $DIST_DIR

git add .

echo "コミットしますか？（続行するなら'y'）"

read answer

if [ "$answer" = "y" ]; then
  git commit || { echo "コミットが失敗しました。"; exit 1; }
fi

echo "リモートにプッシュしますか？（続行するなら'y'）"

read answer

if [ "$answer" = "y" ]; then
  git push $REMOTE_NAME $BRANCH_NAME || { echo "プッシュが失敗しました。"; exit 1; }
fi

echo "操作が完了しました。"

exit 0
