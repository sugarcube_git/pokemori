import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import Sort from '@/components/Sort3'
import Table from '@/components/Table'

import Test from '@/components/Test'

Vue.use(BootstrapVue)
Vue.use(VueRouter)

export default new VueRouter({
  //mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Sort',
      component: Sort
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    },
    {
      path: '/kaeru',
      name: 'Table',
      component: Table
    }
  ]
})
